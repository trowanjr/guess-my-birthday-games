from random import randint

name = input("Hi! What is your name? ")

for guess_number in range(1,6):
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)
    birthday_guess = (guess_month, guess_year)

    print("Guess", name, "were you born in ", birthday_guess, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I give up!")
    else:
        print("Let me guess again.")
